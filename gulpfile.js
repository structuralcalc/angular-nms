var gulp = require('gulp');
var concat = require('gulp-concat');
var bower = require('gulp-bower');
var rename = require("gulp-rename");
var templateCache = require('gulp-angular-templatecache');
var sass = require('gulp-sass');

var SRC_ROOT = './src/';
var BUILD_ROOT = './dist/';
var JS_BUILD_ROOT = BUILD_ROOT;
var FONT_BUILD_ROOT = BUILD_ROOT + 'fonts/';
var VENDOR_ROOT = './node_modules/';
var MINIFIED_LIBS = false;

var jsLibs = [
    'jquery/dist/jquery',
    'angular/angular',
    'angular-animate/angular-animate',
    'angular-aria/angular-aria',
    'angular-route/angular-route',
    'angular-touch/angular-touch',
    'angular-material/angular-material',
    'angular-messages/angular-messages',
    'oclazyload/dist/ocLazyLoad',
    'angular-file-saver/dist/angular-file-saver',
    'angular-file-saver/dist/angular-file-saver.bundle',
    'angular-ui-codemirror/codemirror/lib/codemirror',
    'jsonlint/lib/jsonlint',
    'angular-ui-codemirror/codemirror/mode/javascript/javascript',
    'angular-ui-codemirror/codemirror/mode/shell/shell',
    'angular-ui-codemirror/codemirror/addon/lint/lint',
    'angular-ui-codemirror/codemirror/addon/lint/json-lint',
    'angular-ui-codemirror/src/ui-codemirror',
    'three/three',
    'ngmap/build/scripts/ng-map'
];

var cssLibs = [
    'angular-material/angular-material',
    'angular-resizable/src/angular-resizable',
    'font-awesome/css/font-awesome',
    'angular-ui-codemirror/codemirror/lib/codemirror',
    'angular-ui-codemirror/codemirror/addon/lint/lint',
    'angular-ui-codemirror/codemirror/theme/twilight'
];

var fontLibPaths = [
    'angular-material',
    'font-awesome'
];

var jsSource = [
    'angular-nms.js',
    'services/**/*.js',
    'directives/**/*.js',
    '**/*.js'
];

jsLibs = jsLibs.map(function(path) {
    return VENDOR_ROOT + path + '.js';
});

var fontLibs = [];
fontLibPaths.forEach(function(path) {
    fontLibs.push(VENDOR_ROOT + path + '/**/*.otf');
    fontLibs.push(VENDOR_ROOT + path + '/**/*.eot');
    fontLibs.push(VENDOR_ROOT + path + '/**/*.ttf');
    fontLibs.push(VENDOR_ROOT + path + '/**/*.woff');
});

cssLibs = cssLibs.map(function(path) {
    return VENDOR_ROOT + path + '.css';
});

jsSource = jsSource.map(function(path) {
    return SRC_ROOT + path;
});

gulp.task('default', ['script-lib', 'editor-lib', 'sass', 'editor-sass']);

gulp.task('templates', function () {

    // return to avoid race condition with scripts task
    return gulp.src([SRC_ROOT + '**/*.html'])
            .pipe(templateCache('templates.js', {
                module: 'nmsTemplates',
                standalone: true
            }))
            .pipe(gulp.dest(SRC_ROOT));
});

gulp.task('bower-codemirror', function() {
  return bower({ directory: './node_modules/angular-ui-codemirror', cwd: './node_modules/angular-ui-codemirror' })
    .pipe(gulp.dest('./node_modules/angular-ui-codemirror'));
});

gulp.task('copy', ['script-lib', 'editor-lib', 'sass', 'editor-sass'], function() {
    gulp.src('dist/**/*.*')
    .pipe(gulp.dest('../nms/node_modules/angular-nms/dist/'));
});

gulp.task('sass', ['font-libs'], function () {
    return gulp.src(cssLibs.concat([SRC_ROOT + '**/*.scss', SRC_ROOT + '**/*.css']))
            .pipe(sass())
            .pipe(concat('angular-nms.css'))
            .pipe(gulp.dest(JS_BUILD_ROOT));
});

gulp.task('font-libs', function() {
    return gulp.src(fontLibs)
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(FONT_BUILD_ROOT));
});

gulp.task('script-lib', ['templates', 'bower-codemirror'], function() {
    return gulp.src(jsLibs.concat(jsSource))
        .pipe(concat('angular-nms.js'))
        .pipe(gulp.dest(JS_BUILD_ROOT));
});

gulp.task('editor-sass', [], function () {
    return gulp.src(EDITOR_CSS_LIBS)
            .pipe(sass())
            .pipe(concat('angular-nms-editor.css'))
            .pipe(gulp.dest(JS_BUILD_ROOT));
});

gulp.task('editor-lib', [], function() {
    return gulp.src(EDITOR_JS_LIBS)
        .pipe(concat('angular-nms-editor.js'))
        .pipe(gulp.dest(JS_BUILD_ROOT));
});



var EDITOR_CSS_LIBS = [
    "editor/css/main.css",
    "editor/css/light.css",
    "editor/js/libs/codemirror/codemirror.css",
    "editor/js/libs/codemirror/theme/monokai.css",
    "editor/js/libs/codemirror/addon/dialog.css",
    "editor/js/libs/codemirror/addon/show-hint.css",
    "editor/js/libs/codemirror/addon/tern.css"
];

var EDITOR_JS_LIBS = [
    "editor/three/examples/js/libs/system.min.js",
    "editor/three/examples/js/controls/EditorControls.js",
    "editor/three/examples/js/controls/TransformControls.js",
    "editor/three/examples/js/libs/jszip.min.js",
    "editor/three/examples/js/loaders/AMFLoader.js",
    "editor/three/examples/js/loaders/AWDLoader.js",
    "editor/three/examples/js/loaders/BabylonLoader.js",
    "editor/three/examples/js/loaders/ColladaLoader2.js",
    "editor/three/examples/js/loaders/FBXLoader.js",
    "editor/three/examples/js/loaders/KMZLoader.js",
    "editor/three/examples/js/loaders/MD2Loader.js",
    "editor/three/examples/js/loaders/OBJLoader.js",
    "editor/three/examples/js/loaders/PlayCanvasLoader.js",
    "editor/three/examples/js/loaders/PLYLoader.js",
    "editor/three/examples/js/loaders/STLLoader.js",
    "editor/three/examples/js/loaders/UTF8Loader.js",
    "editor/three/examples/js/loaders/VRMLLoader.js",
    "editor/three/examples/js/loaders/VTKLoader.js",
    "editor/three/examples/js/loaders/ctm/lzma.js",
    "editor/three/examples/js/loaders/ctm/ctm.js",
    "editor/three/examples/js/loaders/ctm/CTMLoader.js",
    "editor/three/examples/js/exporters/OBJExporter.js",
    "editor/three/examples/js/exporters/STLExporter.js",
    "editor/three/examples/js/loaders/deprecated/SceneLoader.js",
    "editor/three/examples/js/renderers/Projector.js",
    "editor/three/examples/js/renderers/CanvasRenderer.js",
    "editor/three/examples/js/renderers/RaytracingRenderer.js",
    "editor/three/examples/js/renderers/SoftwareRenderer.js",
    "editor/three/examples/js/renderers/SVGRenderer.js",

    "editor/js/libs/codemirror/codemirror.js",

    "editor/js/libs/codemirror/mode/javascript.js",
    "editor/js/libs/esprima.js",
    "editor/js/libs/jsonlint.js",
    "editor/js/libs/glslprep.min.js",

    "editor/js/libs/codemirror/mode/glsl.js",

    "editor/js/libs/codemirror/addon/dialog.js",
    "editor/js/libs/codemirror/addon/show-hint.js",
    "editor/js/libs/codemirror/addon/tern.js",
    "editor/js/libs/acorn/acorn.js",
    "editor/js/libs/acorn/acorn_loose.js",
    "editor/js/libs/acorn/walk.js",
    "editor/js/libs/ternjs/polyfill.js",
    "editor/js/libs/ternjs/signal.js",
    "editor/js/libs/ternjs/tern.js",
    "editor/js/libs/ternjs/def.js",
    "editor/js/libs/ternjs/comment.js",
    "editor/js/libs/ternjs/infer.js",
    "editor/js/libs/ternjs/doc_comment.js",
    "editor/js/libs/tern-threejs/threejs.js",

    "editor/js/libs/signals.min.js",
    "editor/js/libs/ui.js",
    "editor/js/libs/ui.three.js",

    "editor/js/libs/app.js",
    "editor/js/Player.js",
    "editor/js/Script.js",

    "editor/three/examples/js/effects/VREffect.js",
    "editor/three/examples/js/controls/VRControls.js",

    "editor/js/Storage.js",

    "editor/js/Editor.js",
    "editor/js/Config.js",
    "editor/js/History.js",
    "editor/js/Loader.js",
    "editor/js/Menubar.js",
    "editor/js/Menubar.File.js",
    "editor/js/Menubar.Edit.js",
    "editor/js/Menubar.Add.js",
    "editor/js/Menubar.Play.js",
    "editor/js/Menubar.Examples.js",
    "editor/js/Menubar.Help.js",
    "editor/js/Menubar.Status.js",
    "editor/js/Sidebar.js",
    "editor/js/Sidebar.Scene.js",
    "editor/js/Sidebar.Project.js",
    "editor/js/Sidebar.Settings.js",
    "editor/js/Sidebar.Properties.js",
    "editor/js/Sidebar.Object.js",
    "editor/js/Sidebar.Geometry.js",
    "editor/js/Sidebar.Geometry.Geometry.js",
    "editor/js/Sidebar.Geometry.BufferGeometry.js",
    "editor/js/Sidebar.Geometry.Modifiers.js",
    "editor/js/Sidebar.Geometry.BoxGeometry.js",
    "editor/js/Sidebar.Geometry.CircleGeometry.js",
    "editor/js/Sidebar.Geometry.CylinderGeometry.js",
    "editor/js/Sidebar.Geometry.IcosahedronGeometry.js",
    "editor/js/Sidebar.Geometry.PlaneGeometry.js",
    "editor/js/Sidebar.Geometry.SphereGeometry.js",
    "editor/js/Sidebar.Geometry.TorusGeometry.js",
    "editor/js/Sidebar.Geometry.TorusKnotGeometry.js",
    "editor/three/examples/js/geometries/TeapotBufferGeometry.js",
    "editor/js/Sidebar.Geometry.TeapotBufferGeometry.js",
    "editor/js/Sidebar.Geometry.LatheGeometry.js",
    "editor/js/Sidebar.Material.js",
    "editor/js/Sidebar.Animation.js",
    "editor/js/Sidebar.Script.js",
    "editor/js/Sidebar.History.js",
    "editor/js/Toolbar.js",
    "editor/js/Viewport.js",
    "editor/js/Viewport.Info.js",
    "editor/js/Command.js",
    "editor/js/commands/AddObjectCommand.js",
    "editor/js/commands/RemoveObjectCommand.js",
    "editor/js/commands/MoveObjectCommand.js",
    "editor/js/commands/SetPositionCommand.js",
    "editor/js/commands/SetRotationCommand.js",
    "editor/js/commands/SetScaleCommand.js",
    "editor/js/commands/SetValueCommand.js",
    "editor/js/commands/SetUuidCommand.js",
    "editor/js/commands/SetColorCommand.js",
    "editor/js/commands/SetGeometryCommand.js",
    "editor/js/commands/SetGeometryValueCommand.js",
    "editor/js/commands/MultiCmdsCommand.js",
    "editor/js/commands/AddScriptCommand.js",
    "editor/js/commands/RemoveScriptCommand.js",
    "editor/js/commands/SetScriptValueCommand.js",
    "editor/js/commands/SetMaterialCommand.js",
    "editor/js/commands/SetMaterialValueCommand.js",
    "editor/js/commands/SetMaterialColorCommand.js",
    "editor/js/commands/SetMaterialMapCommand.js",
    "editor/js/commands/SetSceneCommand.js",
    "editor/editor.js"
];

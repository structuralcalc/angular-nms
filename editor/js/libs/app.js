/**
 * @author mrdoob / http://mrdoob.com/
 */

var APP = {

	Player: function (parentElem, selectedModule) {

		var scope = this;
		this.parentElem = parentElem;
		var module = selectedModule;

		var loader = new THREE.ObjectLoader();
		var camera, scene, renderer;

		var vr, controls, effect;

		var events = {};

		this.dom = undefined;

		this.width = 500;
		this.height = 500;

		this.load = function ( json ) {

			vr = json.project.vr;

			renderer = new THREE.WebGLRenderer( { antialias: true } );
			renderer.setClearColor( 0x000000 );
			renderer.setPixelRatio( window.devicePixelRatio );

			if ( json.project.shadows ) {

				renderer.shadowMap.enabled = true;
				// renderer.shadowMap.type = THREE.PCFSoftShadowMap;

			}

			this.dom = renderer.domElement;

			this.setScene( loader.parse( json.scene ) );
			this.setCamera( loader.parse( json.camera ) );

			events = {
				init: [],
				start: [],
				stop: [],
				keydown: [],
				keyup: [],
				mousedown: [],
				mouseup: [],
				mousemove: [],
				touchstart: [],
				touchend: [],
				touchmove: [],
				update: []
			};

			var scriptWrapParams = 'player,renderer,scene,camera,module';
			var scriptWrapResultObj = {};

			for ( var eventKey in events ) {

				scriptWrapParams += ',' + eventKey;
				scriptWrapResultObj[ eventKey ] = eventKey;

			}

			var scriptWrapResult = JSON.stringify( scriptWrapResultObj ).replace( /\"/g, '' );

			for ( var uuid in json.scripts ) {

				var object = scene.getObjectByProperty( 'uuid', uuid, true );

				if ( object === undefined ) {

					console.warn( 'APP.Player: Script without object.', uuid );
					continue;

				}

				var scripts = json.scripts[ uuid ];

				for ( var i = 0; i < scripts.length; i ++ ) {

					var script = scripts[ i ];

					var functions = ( new Function( scriptWrapParams, script.source + '\nreturn ' + scriptWrapResult + ';' ).bind( object ) )( this, renderer, scene, camera, module );

					for ( var name in functions ) {

						if ( functions[ name ] === undefined ) continue;

						if ( events[ name ] === undefined ) {

							console.warn( 'APP.Player: Event type not supported (', name, ')' );
							continue;

						}

						events[ name ].push( functions[ name ].bind( object ) );

					}

				}

			}

			dispatch( events.init, arguments );

		};

		this.setCamera = function ( value ) {

			camera = value;
			camera.aspect = this.width / this.height;
			camera.updateProjectionMatrix();

			if ( vr === true ) {

				if ( camera.parent === null ) {

					// camera needs to be in the scene so camera2 matrix updates

					scene.add( camera );

				}

				var camera2 = camera.clone();
				camera.add( camera2 );

				camera = camera2;

				controls = new THREE.VRControls( camera );
				effect = new THREE.VREffect( renderer );

				this.parentElem.addEventListener( 'keyup', function ( event ) {

					switch ( event.keyCode ) {
						case 90:
							controls.zeroSensor();
							break;
					}

				} );

				this.dom.addEventListener( 'dblclick', function () {

					effect.setFullScreen( true );

				} );

			}

		};

		this.setScene = function ( value ) {

			scene = value;

		};

		this.setSize = function ( width, height ) {

			if ( renderer._fullScreen ) return;

			this.width = width;
			this.height = height;

			camera.aspect = this.width / this.height;
			camera.updateProjectionMatrix();

			renderer.setSize( width, height );

		};

		function dispatch( array, event ) {

			for ( var i = 0, l = array.length; i < l; i ++ ) {

				array[ i ]( event );

			}

		}

		var prevTime, request;

		function animate( time ) {

			request = requestAnimationFrame( animate );

			try {

				dispatch( events.update, { time: time, delta: time - prevTime } );

			} catch ( e ) {

				console.error( ( e.message || e ), ( e.stack || "" ) );

			}

			if ( vr === true ) {

				controls.update();
				effect.render( scene, camera );

			} else {

				renderer.render( scene, camera );

			}

			prevTime = time;

		}

		this.play = function () {

			this.parentElem.addEventListener( 'keydown', onElementKeyDown );
			this.parentElem.addEventListener( 'keyup', onElementKeyUp );
			this.parentElem.addEventListener( 'mousedown', onElementMouseDown );
			this.parentElem.addEventListener( 'mouseup', onElementMouseUp );
			this.parentElem.addEventListener( 'mousemove', onElementMouseMove );
			this.parentElem.addEventListener( 'touchstart', onElementTouchStart );
			this.parentElem.addEventListener( 'touchend', onElementTouchEnd );
			this.parentElem.addEventListener( 'touchmove', onElementTouchMove );

			dispatch( events.start, arguments );

			request = requestAnimationFrame( animate );
			prevTime = performance.now();

		};

		this.stop = function () {

			this.parentElem.removeEventListener( 'keydown', onElementKeyDown );
			this.parentElem.removeEventListener( 'keyup', onElementKeyUp );
			this.parentElem.removeEventListener( 'mousedown', onElementMouseDown );
			this.parentElem.removeEventListener( 'mouseup', onElementMouseUp );
			this.parentElem.removeEventListener( 'mousemove', onElementMouseMove );
			this.parentElem.removeEventListener( 'touchstart', onElementTouchStart );
			this.parentElem.removeEventListener( 'touchend', onElementTouchEnd );
			this.parentElem.removeEventListener( 'touchmove', onElementTouchMove );

			dispatch( events.stop, arguments );

			cancelAnimationFrame( request );

		};

		//

		function onElementKeyDown( event ) {

			dispatch( events.keydown, event );

		}

		function onElementKeyUp( event ) {

			dispatch( events.keyup, event );

		}

		function onElementMouseDown( event ) {

			dispatch( events.mousedown, event );

		}

		function onElementMouseUp( event ) {

			dispatch( events.mouseup, event );

		}

		function onElementMouseMove( event ) {

			dispatch( events.mousemove, event );

		}

		function onElementTouchStart( event ) {

			dispatch( events.touchstart, event );

		}

		function onElementTouchEnd( event ) {

			dispatch( events.touchend, event );

		}

		function onElementTouchMove( event ) {

			dispatch( events.touchmove, event );

		}

	}

};

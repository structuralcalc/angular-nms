angular.module('NMS')
.constant('nmsAPI', {
	ls: '/ls',
	calculations: '/calculations'
})
.provider('nmsRegistry', function() {
	var config = {
		root: ''
	};

	this.configure = function configure(configuration) {
		config.root = configuration.root;
	};

	this.$get = ['$http', 'nmsAPI', 'NmsModule', function nmsRegistry($http, nmsAPI, NmsModule) {
		return {
			ls: function ls() {
				return $http.get(config.root + nmsAPI.ls)
				.then(function(response) {
					return response.data.calculations;
				});
			},
			config: config,
			getModule: function getModule(id) {
				return $http.get(config.root + nmsAPI.calculations + '/' + id)
				.then(function(response) {
					return new NmsModule(response.data, config.root);
				});
			}
		};
	}];
});
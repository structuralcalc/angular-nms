angular.module('NMS')
.factory('NmsPdf', ['FileSaver', 'Blob', function(FileSaver, Blob) {

	function b64toBlob(b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;

		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
			  byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);

			byteArrays.push(byteArray);
		}

		var blob = new Blob(byteArrays, {type: contentType});
		return blob;
	}

	function NmsPdf(base64Bytes) {
		this.blob = b64toBlob(base64Bytes, 'application/pdf');
	}

	NmsPdf.prototype.saveAs = function saveAs(fileName) {
		FileSaver.saveAs(this.blob, fileName);
	};

	return NmsPdf;
}])
.provider('nmsLatex', function() {
	var config = {
		root: ''
	};

	this.configure = function configure(configuration) {
		config.root = configuration.root;
	};

	this.$get = ['$q', '$log', '$http', 'NmsPdf', function nmsRegistry($q, $log, $http, NmsPdf) {
		
		function compile(body, images) {
			return $http.post(config.root + '/compile', {
				body: body,
				images: images || []
			})
			.catch(function(response) {
				$log.error('[nmsLatex]: ' + response.data.message);
				return $q.reject(response.data);
			})
			.then(function(response) {
				return new NmsPdf(response.data.pdfBase64);
			});
			
		}

		return {
			compile: compile
		};
	}];
});
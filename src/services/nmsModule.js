angular.module('NMS')
.factory('NmsModule', ['$ocLazyLoad', '$compile', '$http', '$q', function($ocLazyLoad, $compile, $http, $q) {
	function Module(definition, root) {
		this.definition = definition;
		this.root = root;
	}

	Module.prototype.load = function load(scope, locatorOrElement, debug) {
		var self = this;
		function loadContent(html) {
			var element = angular.isString(locatorOrElement) ? angular.element($(locatorOrElement)) : locatorOrElement;
			element.empty();
			element.html(html);
			$compile(element.contents())(scope);
		}

		if (debug) {
			loadContent('<nms-debug-module flex layout="column"></nms-debug-module>');
			return $q.when(true);
		}
        $http.get(self.root + self.definition.frontendLink + '/load.json')
        .catch(function(response) {
            if (response.status === 404) {
                return $q.when([]);
            }
        })
        .then(function(response) {
            return $ocLazyLoad.load([
    			self.root + self.definition.frontendLink + '/module.js',
    			self.root + self.definition.frontendLink + '/module.css',
    			self.root + self.definition.frontendLink + '/module.html',
    		].concat((response.data || []).map(function(requiredPath) {
                if (requiredPath.indexOf('/') === 0) {
                    return self.root + requiredPath;
                }
                return self.root + self.definition.frontendLink + '/' + requiredPath;
            })));
        })
		.then(function() {
			loadContent('<' + self.definition.id + '></' + self.definition.id + '>');
		})
		.catch(function(error) {
			loadContent('<p>' + error + '</p>');
		});
	};

	Module.prototype.run = function run(inputs) {
		return $http.post(this.root + this.definition.selfLink, inputs)
		.catch(function(response) {
			if (!angular.isObject(response.data) || !response.data.message) {
				response.data = {
					message: 'Error encountered while running this calculation'
				};
			}
			return $q.reject(response.data);
		})
		.then(function(response) {
			return response.data;
		});
	};

	Module.prototype.getDefinition = function getDefinition() {
		return this.definition;
	};

	return Module;
}]);

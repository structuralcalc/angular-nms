angular.module('NMS')
.factory('generateRenderer', function() {
    return function generateRenderer(width, height) {
        var r = new THREE.WebGLRenderer({
						alpha:true,
					 	preserveDrawingBuffer:true,
						antialias: true
				 });
        r.setSize(width, height);
        r.setClearColor(0x000000, 0 );
        return r;
    }
})
.directive('threeScene', ['generateRenderer', function(generateRenderer) {
    return {
        restrict: 'E',
        scope: {
            inputs: '=', // inputs that will be passed as second arg to renderFunc
            renderFunc: '=' // function that should take two arguments (THREE.Renderer, inputs)
        },
        link: function link(scope, elem, attrs) {
            var sceneWidth = elem[0].clientWidth;
            var sceneHeight = elem[0].clientHeight;
            var renderer = generateRenderer(sceneWidth, sceneHeight, elem);
            elem.append(renderer.domElement);
            scope.$watch('inputs', function() {
                elem.children().not(':first').remove()
                scope.renderFunc(renderer, scope.inputs, elem);
            }, true)
        }
    };
}]);

angular.module('NMS')
.directive('plusButton', [
	function() {
		return {
			restrict: 'E',
			template: '<i class="plusButton fa fa-plus-square fa-2x" style="cursor: pointer"></i>'
		};
	}
]);
angular.module('NMS')
.directive('minusButton', [
	function() {
		return {
			restrict: 'E',
			template: '<i class="minusButton fa fa-minus-square fa-2x" style="cursor: pointer"></i>'
		};
	}
]);
angular.module('NMS')
.directive('nmsScene', ['Player', '$timeout', function(Player, $timeout) {
	return {
		restrict: 'E',
		scope: {
			link: '=',
			module: '='
		},
		link: function(scope, elem) {
			var resizeLoop = angular.noop;
			var jElem = $(elem);
			var lastWidth = 0;
			var lastHeight = 0;
			jElem.parent().css('position', 'relative');
			elem.css('position', 'absolute');
			elem.css('width', '100%');
			elem.css('position', 'absolute');

			function load() {
				if (!scope.link && !scope.module) {
					return;
				}
				var loader = new THREE.XHRLoader();
				loader.load(scope.link, function ( text ) {

					var json = JSON.parse( text );
					var player = new Player(jElem, scope.module);
					player.load( json );
					player.setSize( jElem.width(), jElem.height() );
					player.play();

					elem.append( player.dom );

					elem.on('resize', function () {
						player.setSize( jElem.width(), jElem.height() );
					} );

				});

				function loopFunc() {
					resizeLoop = $timeout(function() {
						var newHeight = jElem.height();
						var newWidth = jElem.width();
						if (newHeight !== lastHeight || newWidth !== lastWidth) {
							elem.trigger('resize');
						}
						loopFunc();
					}, 100);
				}

				loopFunc();
			}

			scope.$watch('link', load);
			scope.$watch('module', load);

			scope.$on('$destroy', function() {
				resizeLoop();
			});
		}
	};
}]);
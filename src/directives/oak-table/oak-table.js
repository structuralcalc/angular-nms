angular.module('oakTable', ['nmsTemplates'])
.constant('oakTable.constants', {
	DEFAULTS: {
		CELL: {
			WIDTH: 100,
			HEIGHT: 30
		},
        TYPES_EMPTY: {
            'Number': NaN,
            'String': '',
            'Object': {}
        }
	}
})
.factory('OakTableApi', function() {
	function OakTableApi(ctrl) {
		this._ctrl = ctrl;
	}

	OakTableApi.prototype.pushRow = function pushRow(row) {
		this._ctrl.pushRow(row);
	};

	return OakTableApi;
})
.directive('oakBindEntity', [
	function() {
		return {
			restrict: 'AE',
			link: function(scope, elem, attrs) {

				function update() {
					elem.empty();
					elem.append(scope.$eval(scope.$eval(attrs.oakBindEntity), {
						'$rowindex': scope.cell.row.index,
						'$columnindex': scope.cell.column.index
					}));
				}

				scope.$watch(function() {
					return scope.cell.row.index;
				}, update);

				scope.$watch(function() {
					return scope.cell.column.index;
				}, update);

				scope.$watch(function() {
					return attrs.oakBindEntity;
				}, update);
			}
		};
	}
])
.directive('oakTableCell', [
	function() {
		return {
			restrict: 'AE',
			require: ['^oakTable', 'oakTableCell', 'ngModel'],
			controller: function($scope, $element) {
				var self = this;

				self.focus = function focus() {
					delete $scope.cell.setFocusOnLink;
					$element.focus();
				};
			},
			link: function(scope, elem, attrs, ctrls) {
				var oakTable = ctrls[0];
				var oakTableCell = ctrls[1];
				var ngModelCtrl = ctrls[2];
				var proxyCell = angular.isDefined(attrs.oakTableCellProxy);

				oakTable.registerCellCtrl(oakTableCell, scope.cell.row.index, scope.cell.column.index);

				if (scope.cell.setFocusOnLink) {
					oakTableCell.focus();
				}

				function read(model) {
					oakTable.setCellValue(model, scope.cell.row.index, scope.cell.column.index);
				}

				scope.$watch(function() {
					return ngModelCtrl.$modelValue;
				}, read);

				elem.bind({
					keydown: function(e) {
						if (e.keyCode === 13) {
							oakTable.focusLastRowColumn(scope.cell.row.index);
							return false;
						}
					}
				});

				elem.bind('click', function() {
					oakTable.setColumnIndex(scope.cell.column.index);
                    if (angular.isDefined(attrs.lastRow)) {
                        oakTable.focusLastRowColumn(oakTable.rows.length - 1);
                    }
				});

                elem.bind('focus', function() {
                    oakTable.setColumnIndex(scope.cell.column.index);
                    if (angular.isDefined(attrs.lastRow)) {
                        oakTable.focusLastRowColumn(oakTable.rows.length - 1);
                    }
				});
			}
		};
	}
])
.directive('oakTable', ['$log', 'oakTable.constants', 'OakTableApi', '$timeout', function($log, constants, OakTableApi, $timeout) {
	return {
		restrict: 'EA',
		bindToController: true,
		scope: {
			config: '=oakTable'
		},
		templateUrl: 'directives/oak-table/oak-table.html',
		require: 'oakTable',
		controllerAs: 'ctrl',
		controller: function($scope) {
			var self = this;
			var lastCursorColumn = 0;
			self.constants = constants;
			self.headerRows = [];
			self.rows = [];
			self.columns = [];
            self.lastRow = [];

			self.registerCellCtrl = function registerCellCtrl(cellCtrl, rowIndex, columnIndex) {
                if (!self.rows[rowIndex]) {
                    return;
                }
				self.rows[rowIndex][columnIndex].cellCtrl = cellCtrl;
			};

			self.focusLastRowColumn = function focusLastRowColumn(rowIndex) {
				if (angular.isDefined(rowIndex) && rowIndex < self.rows.length - 1) {
					self.rows[rowIndex + 1][lastCursorColumn].cellCtrl.focus();
					return;
				}
				var emptyRow = [];
				for (var i = 0; i < self.columns.length; i++) {
                    var options = self.getColumnOptions(i);
					emptyRow.push({entity: constants[options.type]});
				}
				self.pushRow(emptyRow);
				self.rows[rowIndex + 1][lastCursorColumn].setFocusOnLink = true;
				$timeout(function(){});
			};

			self.setColumnIndex = function(columnIndex) {
				lastCursorColumn = columnIndex;
			};

            self.getColumnOptions = function getColumnOptions(columnIndex) {
                return self.headerRows[0] && self.headerRows[0][columnIndex] ? self.headerRows[0][columnIndex].options || {} : {};
            }

            self.applyType = function(value, columnIndex) {
                var options = self.getColumnOptions(columnIndex);
				if (options.type) {
					value = window[options.type](value);
				}
                return value;
            }

			self.setCellValue = function setCellValue(value, rowIndex, columnIndex) {
                if (!self.config.data[rowIndex]) {
                    return;
                }
				value = self.applyType(value, columnIndex);
				self.config.data[rowIndex][columnIndex].entity = value;
			};

			self.getMaxBodyHeight = function getMaxBodyHeight() {
				return self.config.maxHeight || 300;
			};

			self.getColumn = function getColumn(index) {
				if (index === self.columns.length) {
					var col = {
						index: index
					};
					self.columns.push(col);
					return col;
				}
				return self.columns[index];
			};

			self.getRow = function getRow(index) {
				if (index === self.rows.length) {
					var row = {
						index: index
					};
					self.rows.push(row);
					return row;
				}
				return self.rows[index];
			};

			self.getRowHeader = function getRowHeader(index) {
				if (index === self.headerRows.length) {
					var row = {
						index: index
					};
					self.headerRows.push(row);
					return row;
				}
				return self.headerRows[index];
			};

			self.popRow = function popRow(rowIndex) {
				self.rows.splice(rowIndex, 1);
				self.config.data.splice(rowIndex, 1);
                self.rows = self.rows.map(function(row, index) {
                    return row.map(function(cell) {
                        cell.row.index = index;
                        return cell;
                    });
                });
                self.updateLastRow();
			};

			self.pushRow = function pushRow(row) {
				var index = self.rows.length;
				if (self.config.data[index]) {
					self.config.data[index] = row;
				} else {
					self.config.data.push(row);
				}
				var rowDef = self.getRow(index);
				var internalRow = row.map(function(cell, colIndex) {
					var col = self.getColumn(colIndex);
					cell.options = cell.options || {};
					return {
						entity: cell.entity,
						options: cell.options,
						column: col,
						row: rowDef
					};
				});
				self.rows[index] = internalRow;
                self.updateLastRow();
			};

            self.updateLastRow = function updateLastRow() {
                self.lastRow = angular.copy(self.rows[self.rows.length - 1]);
                self.lastRow = self.lastRow.map(function(c) {
                    var cell = angular.copy(c);
                    cell.row.index = self.rows.length;
                    return cell;
                });
            }

			self.onConfigChange = function onConfigChange(config) {
				self.config = config;
                lastCursorColumn = 0;
    			self.headerRows = [];
    			self.rows = [];
    			self.columns = [];
                self.lastRow = [];
				self.allRequired = config.allRequired;
				self.config.data = self.config.data || [];
				self.config.header = self.config.header || [];

				self.config.data.forEach(function(row) {
					self.pushRow(row);
				});

				self.headerRows = self.config.header.map(function(row, rowIndex) {
					var rowDef = self.getRowHeader(rowIndex);
					return row.map(function(cell, colIndex) {
						var col = self.getColumn(colIndex);
						cell.options = cell.options || {};
						col.readOnly = cell.options.readOnly;
						col.rowEntity = cell.rowEntity;
						return {
							entity: cell.entity,
							options: cell.options,
							column: col,
							row: rowDef
						};
					});
				});
                self.updateLastRow();
			};
		},
		link: function(scope, elem, attrs, ctrl) {
            function init() {
				if (ctrl.config) {
					ctrl.onConfigChange(ctrl.config);
					if (angular.isFunction(ctrl.config.onRegister)) {
						ctrl.config.onRegister(new OakTableApi(ctrl));
					}
				}
			}

			scope.$watch('ctrl.config', init);

            scope.$watch('ctrl.config.data', init);
		}
	};
}]);

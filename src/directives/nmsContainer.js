angular.module('NMS')
.directive('nmsContainer', [function() {
	return {
		restrict: 'EA',
		require: 'nmsContainer',
		bindToController: true,
		scope: {
			module: '=',
			debug: '=?',
		},
		controllerAs: 'ctrl',
		controller: function($scope) {
			var self = this;

			self.onInit = function onInit(func) {
				func(angular.copy($scope.ctrl.module.definition));
			};

			self.run = function run(inputs) {
				return $scope.ctrl.module.run(inputs);
			};
		},
		link: function(scope, elem, attrs, nmsContainerCtrl) {
			var child;

			function load() {
				if (scope.ctrl.module) {
					if (child) {
						child.$destroy();
					}
					child = scope.$new();
					scope.ctrl.module.load(child, elem, nmsContainerCtrl.debug);
				}
			}

			scope.$watch('ctrl.debug', load);
			scope.$watch('ctrl.module', load);
		}
	};
}]);

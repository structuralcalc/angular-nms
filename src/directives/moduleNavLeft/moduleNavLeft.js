angular.module('NMS')
.directive('moduleNavLeft', [function() {
	return {
		restrict: 'E',
		require: '^nmsContainer',
		templateUrl: 'directives/moduleNavLeft/moduleNavLeft.html',
		replace: 'true',
		scope: {
			tabs: '=',
			frontendLink: '=',
			currentTab: '='
		},
		link: function(scope) {
	
			scope._tabs = [];

			scope.$watch(function() {
				return scope.tabs && scope.frontendLink;
			}, init);

			function init() {

				angular.forEach(scope.tabs, function(tab) {
					scope._tabs.push({
						title: tab,
						url: scope.frontendLink + '/' + tab.toLowerCase().replace(' ', '-') + '.html'

					});
				});

				scope.currentTab = scope._tabs[0].url;
			};

			
    		scope.onClickTab = function (tab) {
        		scope.currentTab = tab.url;
    		}
    
		    scope.isActiveTab = function(tabUrl) {
		        return tabUrl == scope.currentTab;
		    }
		}
	};
}])
angular.module('NMS')
.directive('jsonValid', function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elem, attrs, ngModel) {
			ngModel.$validators.jsonValid = function jsonValid(model) {
				try {
					JSON.parse(model);
				} catch (e) {
					return false;
				}
				return true;
			};
		}
	};
})
.directive('nmsDebugModule', ['$sce', 'nmsLatex', function($sce, nmsLatex) {
	return {
		restrict: 'E',
		require: '^nmsContainer',
		templateUrl: 'directives/nmsDebugModule.html',
		controller: function($scope) {
			$scope.editorOptions = {
		        lineNumbers: true,
		        theme: 'twilight',
		        mode: 'application/json',
		        gutters: ["CodeMirror-lint-markers"],
		        lint: true
		    };

		    $scope.resultOptions = {
		        lineNumbers: true,
		        theme: 'twilight',
		        mode: 'application/json',
		        gutters: ["CodeMirror-lint-markers"],
		        lint: true
		    };

		    $scope.shellOptions = {
		        lineNumbers: true,
		        theme: 'twilight',
		        mode: 'shell'
		    };
		},
		link: function(scope, elem, attrs, nmsContainerCtrl) {
			var mockBase64Image = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
			scope.context = {};

			scope.run = function run() {
				scope.compileError = '';
				scope.pdfUrl = "";
				scope.runError = false;
				scope.isCalculationRunning = true;
				scope.resultOptions.mode = 'application/json';
				nmsContainerCtrl.run(JSON.parse(scope.input))
				.then(function(response) {
					scope.result = JSON.stringify(response.outputs, null, '    ');
					scope.runError = false;
					return nmsLatex.compile(response.outputs.report, [{
						name: 'main',
						dataUri: mockBase64Image
					}])
					.then(function(pdf) {
						scope.runError = false;
						scope.pdfUrl = $sce.trustAsResourceUrl(URL.createObjectURL(pdf.blob));
					})
					.catch(function(response) {
						var errorLines = response.log.split('\n').filter(function(line) {
							return line.indexOf('LaTeX Error') !== -1;
						});
						errorLines = ['Failed to compile report:'].concat(errorLines).concat(['', '', '']).join('\n');
						scope.compileError = errorLines + response.log;
					});
				})
				.catch(function(response) {
					scope.runError = true;
					if (angular.isString(response.message)) {
						scope.resultOptions.mode = 'shell';
						scope.result = response.message;
					} else {
						scope.result = JSON.stringify(response.message, null, '    ');
					}
				})
				.finally(function() {
					scope.isCalculationRunning = false;
				});
			};

			scope.$watch('input', function() {
				try{
					var inputs = JSON.parse(scope.input);
					Object.keys(inputs).forEach(function(key) {
						scope.module.inputs[key] = inputs[key];
					});
				} catch (e) {}
			});
			
			nmsContainerCtrl.onInit(function (module) {
				scope.module = module;
				scope.input = JSON.stringify(module.example || {}, null, '    ');
			});
		}
	};
}]);